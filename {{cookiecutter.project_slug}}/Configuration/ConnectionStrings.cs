namespace {{ cookiecutter.project_slug }}.Configuration {
    public record ConnectionStrings {
        public string? SqlServer { get; set; }
    }
}