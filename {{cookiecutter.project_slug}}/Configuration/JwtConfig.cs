namespace {{ cookiecutter.project_slug }}.Configuration{
    public class JwtConfig{
        public string Secret { get; set; }
    }
}
