using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Options;
using {{ cookiecutter.project_slug }}.Repositories;
using {{ cookiecutter.project_slug }}.Configuration;
using {{ cookiecutter.project_slug }}.Dtos.Requests;
using {{ cookiecutter.project_slug }}.Exceptions.Users;
using {{ cookiecutter.project_slug }}.Entities.Users;
using {{ cookiecutter.project_slug }}.Utils;

namespace {{ cookiecutter.project_slug }}.Controllers
{
    [ApiController]
    [Route("api/token")]
    public class TokenController : ControllerBase
    {
        private readonly IUsersRepository _usersRepository;
        private readonly JwtConfig _jwtConfig;

        public TokenController(IUsersRepository usersRepository, IOptionsMonitor<JwtConfig> jwtConfig)
        {
            this._usersRepository = usersRepository;
            this._jwtConfig = jwtConfig.CurrentValue;
        }

        [HttpPost]
        public async Task<ActionResult> CreateTokenAsync(AuthenticateUserDto authenticateUserDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // If the combination of the User's mal and password match a User in Database
                    if (await this.IsMailAndPasswordValid(authenticateUserDto.Mail, authenticateUserDto.Pass))
                    {
                        // Generate a Token for the User trying to Authentify
                        return new ObjectResult(await GenerateToken(authenticateUserDto.Mail));
                    }
                    else
                        return BadRequest();
                }
                catch (UserNotFoundException ex)
                {
                    return Conflict(new { message = ex.Message });
                }
                catch (PasswordDoesntMatchUserException ex)
                {
                    return Conflict(new { message = ex.Message });
                }
            }
            return BadRequest();
        }

        // Used to check in Database if User exists
        private async Task<bool> IsMailAndPasswordValid(string mail, string pass)
        {

            User user = await _usersRepository.GetUserByMailAsync(mail, true);

            return user.ShaPass.ComparePassword(pass);
        }

        private async Task<dynamic> GenerateToken(string mail)
        {
            User user = await _usersRepository.GetUserByMailAsync(mail);
            var claims = new List<Claim>{
                new Claim(ClaimTypes.Name, mail),
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.Now.AddHours(24)).ToUnixTimeSeconds().ToString())
            };

            var token = new JwtSecurityToken(
                new JwtHeader(
                    new SigningCredentials(
                        new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(_jwtConfig.Secret)),
                            SecurityAlgorithms.HmacSha256
                        )
                    ),
                    new JwtPayload(claims)
                );
            var output = new
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                Mail = mail,
                Username = user.Username
            };

            return output;
        }
    }
}
