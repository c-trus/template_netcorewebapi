using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using {{ cookiecutter.project_slug }}.Dtos.Requests;
using {{ cookiecutter.project_slug }}.Dtos.Responses;
using {{ cookiecutter.project_slug }}.Entities.Users;
using {{ cookiecutter.project_slug }}.Exceptions.Users;
using {{ cookiecutter.project_slug }}.Repositories;
using {{ cookiecutter.project_slug }}.Utils;

namespace {{ cookiecutter.project_slug }}.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        const string ACTION_GETUSER = "GetUser";
        private readonly IConfiguration _config;
        private readonly IUsersRepository _userRepo;

        public UsersController(IConfiguration config, IUsersRepository userRepo)
        {
            _config = config;
            _userRepo = userRepo;
        }

        // GET /users
        [HttpGet]
        public async Task<IActionResult> GetUsersAsync()
        {
            var users = (await _userRepo.GetUsersAsync())
                                    .Select(item => new UserDto(item));
            return Ok(users);
        }

        // GET /user/id={id}
        [HttpGet("{id}")]
        [ActionName(ACTION_GETUSER)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<UserDto>> GetUserAsync(int id)
        {
            try
            {
                User user = await _userRepo.GetUserAsync(id);
                return Ok(new UserDto(user));
            }
            catch (UserNotFoundException ex)
            {
                return Conflict(new { message = ex.Message });
            }    
        }

        // POST /users
        [HttpPost]
        public async Task<ActionResult<UserDto>> CreateUserAsync(CreateUserDto createUserDto)
        {
            if (ModelState.IsValid)
            {
                User user = new()
                {
                    Username = createUserDto.Username,
                    Email = createUserDto.Mail,
                    ShaPass = createUserDto.Pass.HashPassword()
                };

                try
                {
                    int userId = await _userRepo.CreateUserAsync(user);
                    user = await _userRepo.GetUserAsync(userId);
                    UserDto dto = new UserDto(user);

                    return await Task.Run(() => CreatedAtAction(ACTION_GETUSER, new { id = dto.Id }, dto));
                }
                catch (RegisterException re)
                {
                    return Conflict(new { message = re.Message });
                }
            }
            return Conflict();
        }
    }
}
