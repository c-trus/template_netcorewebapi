IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = '{{ cookiecutter.mssql_db }}')
BEGIN
    CREATE DATABASE [{{ cookiecutter.mssql_db }}];
END
GO