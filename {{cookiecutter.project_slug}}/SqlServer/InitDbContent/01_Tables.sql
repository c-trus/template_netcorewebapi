IF NOT EXISTS (SELECT * FROM sys.tables WHERE [name] = N'USERS')
BEGIN
    CREATE TABLE USERS (
    userId          INT NOT NULL IDENTITY,
    email           NVARCHAR(255) NOT NULL,
    username        NVARCHAR(50) NOT NULL,
    shaPass         NVARCHAR(255) NOT NULL,

    CONSTRAINT PK_Users PRIMARY KEY (userId)
    );
END
GO