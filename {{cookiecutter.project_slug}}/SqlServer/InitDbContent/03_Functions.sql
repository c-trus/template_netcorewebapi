CREATE OR ALTER FUNCTION ft_CheckUsernameAlreadyExists
	(@username NVARCHAR(50))
RETURNS BIT
AS
BEGIN
	DECLARE @usernameExists BIT = 0;
	IF EXISTS (SELECT u.userId FROM USERS u WHERE LOWER(u.username) = LOWER(@username))
	BEGIN
		SET @usernameExists = 1;
	END
	RETURN @usernameExists;
END
GO

CREATE OR ALTER FUNCTION ft_CheckMailAlreadyExists 
	(@mail NVARCHAR(255))
RETURNS BIT
AS
BEGIN
	DECLARE @mailExists BIT = 0;
	IF EXISTS (SELECT u.userId FROM USERS u WHERE LOWER(u.email) = LOWER(@mail))
	BEGIN
		SET @mailExists = 1;
	END
	RETURN @mailExists;
END
GO

CREATE OR ALTER FUNCTION ft_GetUserIdByMail
	(@mail NVARCHAR(255))
RETURNS INT
AS
BEGIN
DECLARE @userID INT = 0;
IF EXISTS (SELECT TOP 1 u.userId FROM USERS u WHERE LOWER(u.email) = LOWER(@mail))
BEGIN
	SET @userID = (SELECT TOP 1 u.userId FROM USERS u WHERE LOWER(u.email) = LOWER(@mail));
END
RETURN @userID;
END
GO