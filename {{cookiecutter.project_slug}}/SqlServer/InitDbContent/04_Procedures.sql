CREATE OR ALTER PROCEDURE CreateUser
	@username	NVARCHAR(50),
	@mail		NVARCHAR(255),
	@shaPass	NVARCHAR(255)
AS
DECLARE @return_value INT = 0;
IF dbo.ft_CheckMailAlreadyExists(@mail) = 1
BEGIN
	SET @return_value = -1; -- Mail already exists
END
ELSE IF dbo.ft_CheckUsernameAlreadyExists(@username) = 1
BEGIN
	SET @return_value = -2; -- Username taken
END
ELSE
BEGIN
	INSERT INTO USERS (email, username, shaPass) VALUES
	(@mail, @username, @shaPass);
	SET @return_value = SCOPE_IDENTITY();
END
SELECT @return_value;
GO

CREATE OR ALTER PROCEDURE GetUser
	@userId		INT
AS
SELECT * FROM vw_Users u WHERE u.userId = @userId;
GO

CREATE OR ALTER PROCEDURE GetUserByMail
	@mail NVARCHAR(255)
AS
SELECT * FROM vw_Users u WHERE LOWER(u.email) = LOWER(@mail);
GO

CREATE OR ALTER PROCEDURE GetUsers
AS
SELECT * FROM vw_Users u;
GO

CREATE OR ALTER PROCEDURE CheckPassword
	@id INT,
	@shaPass NVARCHAR(255)
AS
DECLARE @return_value INT = 0;

IF EXISTS (SELECT u.userId FROM USERS u WHERE u.userId = @id)
BEGIN
	IF EXISTS (SELECT u.userId FROM USERS u WHERE u.userId = @id AND u.shaPass = @shaPass)
	BEGIN
		SET @return_value = 1; -- User and Pass match
	END
	ELSE
	BEGIN
		SET @return_value = -2; -- User and Pass don't match
	END
END
ELSE
BEGIN
	SET @return_value = -3 -- User not found
END
SELECT @return_value;
GO

