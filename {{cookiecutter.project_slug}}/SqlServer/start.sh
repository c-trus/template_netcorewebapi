#! bin/bash

echo starting server...
nohup /opt/mssql/bin/sqlservr & 
echo server started.

ok=/bin/true

while $ok 
do 
    sleep 2
    /opt/mssql-tools/bin/sqlcmd -U sa -P {{ cookiecutter.mssql_password }} -q "quit"
    ret=$?
    if test $ret -eq 0
    then
        ok=/bin/false
    fi
done

echo creating db...
/opt/mssql-tools/bin/sqlcmd -U sa -P {{ cookiecutter.mssql_password }} -i "/opt/scripts/CreateDb.sql"
echo db created.

echo generating tables...
cd /opt/scripts/InitDbContent
for f in *.sql; do /opt/mssql-tools/bin/sqlcmd -d {{ cookiecutter.mssql_db }} -U sa -P {{ cookiecutter.mssql_password }} -i "$f"; done
echo db initialized.

wait
