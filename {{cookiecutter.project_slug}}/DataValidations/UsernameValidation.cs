using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace {{ cookiecutter.project_slug }}.Validations.Users
{
    public class UsernameValidation : ValidationAttribute
    {
        public const string USERNAME_ERROR = "Username should be 4-12 characters long and contain only alphanumerical characters.";
        public override bool IsValid(object? value)
        {
            string? username = Convert.ToString(value);

            var reg = new Regex("^([a-zA-Z0-9]){4,12}$");
            if (!string.IsNullOrWhiteSpace(username))
                return reg.IsMatch(username);

            return false;
        }
    }
}
