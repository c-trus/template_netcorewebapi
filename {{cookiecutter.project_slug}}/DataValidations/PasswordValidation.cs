using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace {{ cookiecutter.project_slug }}.DataValidations
{
    public class PasswordValidation : ValidationAttribute
    {
        public const string PASSWORD_ERROR = "Password should be 8-16 characters long and contain at least one number, one uppercase and one lowercase.";
        public override bool IsValid(object? value)
        {
            string? password = Convert.ToString(value);

            var hasNumber = new Regex(@"[0-9]+");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var has8To16Chars = new Regex(@"^.{8,16}$");

            if (!string.IsNullOrWhiteSpace(password))
                return hasNumber.IsMatch(password) && hasLowerChar.IsMatch(password) && hasUpperChar.IsMatch(password) && has8To16Chars.IsMatch(password);

            return false;
        }
    }
}
