using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using {{ cookiecutter.project_slug }}.Exceptions.Users;

namespace {{ cookiecutter.project_slug }}.Utils
{
    public static class Extensions
    {
        #region CRYPTO PASS
        public static string HashPassword(this string password)
        {
            // Create salt
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            // Create the Rfc2898DeriveBytes and get the hash value
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);

            // Combine the salt and password bytes
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            // Turn the combined salt+hash into a string
            return Convert.ToBase64String(hashBytes);
        }
        public static bool ComparePassword(this string shaPassword, string password)
        {
            // Extract the bytes 
            byte[] hashBytes = Convert.FromBase64String(shaPassword);

            // Get the salt
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);

            // Compute the hash on the password the user entered
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);

            // Compare the results
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    throw new PasswordDoesntMatchUserException();
            
            return true;
        }
        #endregion
    }
}
