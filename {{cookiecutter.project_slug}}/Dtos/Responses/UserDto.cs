using {{ cookiecutter.project_slug }}.Entities.Users;

namespace {{ cookiecutter.project_slug }}.Dtos.Responses {
    public record UserDto {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Mail { get; set; }

        public UserDto(User user)
        {
            this.Id = user.UserId;
            this.Username = user.Username;
            this.Mail = user.Email;
        }
    }

}
