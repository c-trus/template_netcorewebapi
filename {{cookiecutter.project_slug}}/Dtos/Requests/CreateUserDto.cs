using System.ComponentModel.DataAnnotations;
using {{ cookiecutter.project_slug }}.DataValidations;
using {{ cookiecutter.project_slug }}.Validations.Users;

namespace {{ cookiecutter.project_slug }}.Dtos.Requests
{
    public record CreateUserDto
    {
        [Required]
        [UsernameValidation(ErrorMessage = UsernameValidation.USERNAME_ERROR)]
        public string? Username { get; set; }

        [Required]
        [EmailAddress]
        public string? Mail { get; set; }

        [Required]
        [PasswordValidation(ErrorMessage = PasswordValidation.PASSWORD_ERROR)]
        public string? Pass { get; set; }

    }
}