using System.ComponentModel.DataAnnotations;
using {{ cookiecutter.project_slug }}.DataValidations;

namespace {{ cookiecutter.project_slug }}.Dtos.Requests {
    public record AuthenticateUserDto{
        [Required]
        [EmailAddress]
        public string? Mail { get; set; }

        [Required]
        [PasswordValidation(ErrorMessage = PasswordValidation.PASSWORD_ERROR)]
        public string? Pass { get; set; }
    }

}
