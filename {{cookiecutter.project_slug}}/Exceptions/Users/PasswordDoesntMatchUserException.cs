namespace {{ cookiecutter.project_slug }}.Exceptions.Users
{
    public class PasswordDoesntMatchUserException : Exception
    {
        public PasswordDoesntMatchUserException(string message = "Invalid combinaison of Email and Password") : base(message) { }
    }
}
