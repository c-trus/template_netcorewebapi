namespace {{ cookiecutter.project_slug }}.Exceptions.Users
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(string message = "User not found !") : base(message) { }
    }
}
