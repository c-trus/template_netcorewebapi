namespace {{ cookiecutter.project_slug }}.Exceptions.Users {
    public class RegisterException : Exception
    {
        public RegisterException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -2:
                    return "Username already taken";
                case -1:
                    return "Mail address already used";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }

}