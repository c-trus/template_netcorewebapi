using {{ cookiecutter.project_slug }}.Entities.Users;

namespace {{ cookiecutter.project_slug }}.Repositories {
    public interface IUsersRepository
    {
        Task<User> GetUserAsync(int id);
        Task<IEnumerable<User>> GetUsersAsync();
        Task<User> GetUserByMailAsync(string mail, bool getShaPassword = false);
        Task<string> GetUserShaPasswordAsync(int id);
        Task<int> CreateUserAsync(User user);
    }

}
