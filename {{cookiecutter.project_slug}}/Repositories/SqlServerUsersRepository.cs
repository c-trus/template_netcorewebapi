using System.Data;
using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Options;
using {{ cookiecutter.project_slug }}.Configuration;
using {{ cookiecutter.project_slug }}.Entities.Users;
using {{ cookiecutter.project_slug }}.Exceptions.Users;

namespace {{ cookiecutter.project_slug }}.Repositories
{
    public class SqlServerUsersRepository : IUsersRepository
    {
        #region PROPS
        private readonly string _database;
        #endregion

        #region INITIALIZATION

        public SqlServerUsersRepository(IOptionsMonitor<ConnectionStrings> connStrings)
        {
            this._database = connStrings.CurrentValue.SqlServer ?? "";
        }
        #endregion

        #region IMPLEMENTATIONS
        public async Task<User> GetUserAsync(int id)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userID", id);
                string sql = $@"EXEC DBO.GetUser @userID";
                User result = await Task.Run(() => cnn.Query<User>(sql, p).FirstOrDefault());

                if (result != null) return result;
                else throw new UserNotFoundException();
            }
        }
        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                string sql = $@"EXEC DBO.GetUsers";
                return await Task.Run(() => cnn.Query<User>(sql));
            }
        }
        public async Task<User> GetUserByMailAsync(string mail, bool getShaPassword = false)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@mail", mail);
                string sql = $@"EXEC DBO.GetUserByMail @mail";
                User result = await Task.Run(() => cnn.Query<User>(sql, p).FirstOrDefault());
                if (result != null)
                {
                    if (getShaPassword)
                        try
                        {
                            result.ShaPass = await GetUserShaPasswordAsync(result.UserId);
                        }
                        catch (UserNotFoundException)
                        {
                            throw new UserNotFoundException();
                        }
                    return result;
                }
                else
                    throw new UserNotFoundException();
            }
        }
        public async Task<string> GetUserShaPasswordAsync(int id)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userID", id);
                string sql = $@"EXEC DBO.GetShaPass @UserId";
                string result = await Task.Run(() => cnn.QueryFirstOrDefaultAsync<string>(sql, p));

                if (!string.IsNullOrWhiteSpace(result)) return result;
                else throw new UserNotFoundException();
            }
        }

        public async Task<int> CreateUserAsync(User user)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@username", user.Username);
                p.Add("@mail", user.Email);
                p.Add("@shaPassword", user.ShaPass);

                string sql = $@"EXEC DBO.CreateUser @username, @mail, @shaPassword";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = cnn.Query<int>(sql, p, trans).FirstOrDefault();
                        if (return_value <= 0) throw new RegisterException(RegisterException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(RegisterException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            await Task.CompletedTask;
            return 0;
        }
        #endregion

        #region PRIVATE

        #endregion
    }
}