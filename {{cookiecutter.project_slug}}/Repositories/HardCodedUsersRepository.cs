using {{ cookiecutter.project_slug }}.Entities.Users;
using {{ cookiecutter.project_slug }}.Exceptions.Users;

namespace {{ cookiecutter.project_slug }}.Repositories
{
    public class HardCodedUsersRepository : IUsersRepository
    {
        #region PROPS
        private List<User> _db = new List<User>();
        #endregion

        #region INITIALIZATION

        #endregion

        #region IMPLEMENTATIONS
        public async Task<int> CreateUserAsync(User user)
        {
            int _id = 1;
            foreach (User _user in _db)
            {
                if (_user.Username == user.Username) throw new RegisterException(RegisterException.GetMessage(-2));
                if (_user.Email == user.Email) throw new RegisterException(RegisterException.GetMessage(-1));
            }
            if (_db.Count > 0)
                _id = _db[_db.Count - 1].UserId + 1;
            user.UserId = _id;

            _db.Add(user);
            return await Task.FromResult(_id);
        }

        public async Task<User> GetUserAsync(int id)
        {
            User _user = await Task.Run(() => (from _u in _db where _u.UserId == id select _u).FirstOrDefault() ?? null);
            if (_user is null) throw new UserNotFoundException();

            return _user;
        }

        public async Task<User> GetUserByMailAsync(string mail, bool getShaPassword = false)
        {
            User _user = await Task.Run(() => (from _u in _db where _u.Email.ToLower() == mail.ToLower() select _u).FirstOrDefault() ?? null);
            if (_user != null)
            {
                if (getShaPassword)
                    try
                    {
                        _user.ShaPass = await GetUserShaPasswordAsync(_user.UserId);
                    }
                    catch (UserNotFoundException)
                    {
                        throw new UserNotFoundException();
                    }
                return _user;
            }
            else
                throw new UserNotFoundException();
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            IEnumerable<User> _users = await Task.Run(() => (from _u in _db select _u));
            return _users;
        }

        public async Task<string> GetUserShaPasswordAsync(int id)
        {
            string _shaPass = await Task.Run(() => (from _u in _db where _u.UserId == id select _u.ShaPass).FirstOrDefault() ?? null);
            if (string.IsNullOrWhiteSpace(_shaPass))
                throw new UserNotFoundException();
            return _shaPass;
        }
        #endregion

        #region PRIVATE

        #endregion
    }
}