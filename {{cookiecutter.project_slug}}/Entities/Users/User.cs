namespace {{ cookiecutter.project_slug }}.Entities.Users
{
    public record User
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string ShaPass { get; set; }
    }
}
