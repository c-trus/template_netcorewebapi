using System.Net;

namespace {{ cookiecutter.project_slug }}.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel(options => { options.Listen(IPAddress.Any, 80); });
                    // webBuilder.UseUrls("http://*:80");
                    webBuilder.UseStartup<Startup>();
                });

        // Update Database when new instance of the Host is created

    }
}
